<%-- 
    Document   : loginAdmin
    Created on : May 28, 2020, 1:18:15 PM
    Author     : Mirciony
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP logadmin Page</title>
    </head>
    <style> 
        #login_form {
            position: absolute;
            top: 20%;
            left: 30%;
            right: 30%;
            bottom: 20%;
            font-size: 18px;
        }

        #f1 {
            background-color: white;
            border-style: solid;
            border-width: 1px;
        }
        .f1_label {
            white-space: nowrap;
        }
    </style> 

    <body> 

        <div id="login_form">
            <form name="f1" method="post" action="login.php" id="f1">
                <table>
                    <tr>
                        <td class="f1_label">User Name :</td><td><input type="text" name="username" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td class="f1_label">Password  :</td><td><input type="password" name="password" value="f"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="login" value="Log In" style="font-size:18px; color:blue;  " />
                        </td>
                    </tr>
                </table>
            </form> 
        </div>

    </body>
</html>

