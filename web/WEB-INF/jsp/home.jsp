<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to my first site on Java!</title>
    </head>

    <body>
        <table style="height: min-content; width:100%">
            <tbody>
                <tr>
                    <td style="width:available; text-align: left;">Hello! This is the default welcome page for a Spring Web MVC project</td>
                    <td style="width:min-content; text-align: right;"><a href="loginAdmin.htm">Login</a></td>
                </tr>
            </tbody>
        </table>
        <tt>index.jsp</tt> <i>, or create your own welcome page then change
            the redirection in</i> <tt>redirect.jsp</tt> <i>to point to the new
            welcome page and also update the welcome-file setting in</i>
        <tt>web.xml</tt>.</p>
</body>
</html>
