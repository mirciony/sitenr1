package com.mirciony.services.models;

/**
 *
 * @author Mirciony
 */
public class Client {
    private int id;
    private String nume;
    private String telefon;
    private String email;
    
     public Client() {
    }

    public Client(int id, String nume, String telefon, String email) {
        this.id = id;
        this.nume = nume;
        this.telefon = telefon;
        this.email = email;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Client{" + "id=" + id + ", nume=" + nume + ", telefon=" + telefon + ", email=" + email + '}';
    }
    
}
