package com.mirciony.services.dao;

import com.mirciony.services.models.Client;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface ClientDaoIntf {
    
    void save(Client d);
    void delete(Client d);
    
    Client findById(int id);
    List<Client> findAll();
}
