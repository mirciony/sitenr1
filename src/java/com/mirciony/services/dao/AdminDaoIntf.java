package com.mirciony.services.dao;

import com.mirciony.services.models.Admin;
import java.util.List;

/**
 *
 * @author Mirciony
 */
public interface AdminDaoIntf {
    void save(Admin d);
    void update(Admin d);
    void delete(Admin d);
    
    Admin findById(int id);
    List<Admin> findAll();
}
